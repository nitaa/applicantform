﻿using CRUDNEW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace CRUDNEW.Controllers
{
    public class PelamarController : Controller
    {
        // GET: Pelamar
        public ActionResult Index()
        {
            List<x_pelamar> pelamarList = new List<x_pelamar>();
            using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                pelamarList = db.x_pelamar.ToList<x_pelamar>();
            }
            return View(pelamarList);
        }

        public ActionResult Create()
        {
            return View(new x_pelamar());
        }

        [HttpPost]
        public ActionResult Create(x_pelamar pelamarmodel)
        {
            using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                db.x_pelamar.Add(pelamarmodel);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            x_pelamar pelamarmodel = new x_pelamar();
            using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                pelamarmodel = db.x_pelamar.Where(x => x.id == id).FirstOrDefault();
            };
            return View(pelamarmodel);
        }

        public ActionResult Edit(int id)
        {
            x_pelamar pelamarmodel = new x_pelamar();
            using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                pelamarmodel = db.x_pelamar.Where(x => x.id == id).FirstOrDefault();
            };
            return View(pelamarmodel);
        }

        [HttpPost]
        public ActionResult Edit(x_pelamar pelamarmodel)
        {
            
            using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                db.Entry(pelamarmodel).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            };
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {

            x_pelamar pelamarmodel = new x_pelamar();
            using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                pelamarmodel = db.x_pelamar.Where(x => x.id == id).FirstOrDefault();
            };
            return View(pelamarmodel);
        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
           using (Applicant_DBEntities db = new Applicant_DBEntities())
            {
                x_pelamar pelamarmodel = db.x_pelamar.Where(x => x.id==id).FirstOrDefault();
                db.x_pelamar.Remove(pelamarmodel);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    

    }
}