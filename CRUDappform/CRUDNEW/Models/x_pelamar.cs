//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRUDNEW.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class x_pelamar
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string AlternativePhoneNumber { get; set; }
        public string Email { get; set; }
        public string PlaceOfBirth { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string LastEducation { get; set; }
        public string University { get; set; }
        public string Major { get; set; }
        public string PositionApply { get; set; }
        public string Source { get; set; }
    }
}
