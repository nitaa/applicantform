﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRUDNEW.Models
{
    public class pelamarmodel
    {
        public string id { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessage = "Nama tidak boleh kosong!")]
        public string Name { get; set; }
        [DisplayName("Mobile Phone Number")]
        [Required(ErrorMessage = "Mobile phone number tidak boleh kosong!")]
        public long MobilePhoneNumber { get; set; }
        [DisplayName("Alternative Phone Number")]
        public long? AlternativePhoneNumber { get; set; }
        [DisplayName("Email")]
        [Required(ErrorMessage = "Email tidak boleh kosong!")]
        public string Email { get; set; }

        [DisplayName("Place Of Birth")]
        [Required(ErrorMessage = "Place Of Birth tidak boleh kosong!")]
        public string PlaceOfBirth { get; set; }
        [DisplayName("Date Of Birth")]
        [Required(ErrorMessage = "Date Of Birth tidak boleh kosong!")]
        public string DateOfBirth { get; set; }
        [DisplayName("Last Education")]
        [Required(ErrorMessage = "Last Education tidak boleh kosong!")]
        public string LastEducation { get; set; }
        [DisplayName("College/University")]
        [Required(ErrorMessage = "College/University tidak boleh kosong!")]
        public string University { get; set; }
        [DisplayName("Major")]
        [Required(ErrorMessage = "Major tidak boleh kosong!")]
        public string Major { get; set; }
        [DisplayName("Position Apply")]
        [Required(ErrorMessage = "Position Apply tidak boleh kosong!")]
        public string PositionApply { get; set; }
        [DisplayName("Source")]
        [Required(ErrorMessage = "Source tidak boleh kosong!")]
        public sel Source { get; set; }
    }

    public enum sel
    {
        CareerCenter,
        Event,
        ReferralProgram
    }
}